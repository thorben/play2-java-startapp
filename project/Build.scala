import sbt._
import Keys._
import PlayProject._

object ApplicationBuild extends Build {

    val appName         = "StartApp"
    val appVersion      = "1.0-SNAPSHOT"

    val appDependencies = Seq(
      ("com.typesafe" %% "play-plugins-mailer" % "2.0.2").withJavadoc().withSources()
      ,("org.mindrot" % "jbcrypt" % "0.3m").withJavadoc().withSources()
      ,("org.unitils" % "unitils-dbunit" % "3.3" % "test").withJavadoc().withSources()
        .exclude("org.slf4j", "slf4j-nop")
  )

    val main = PlayProject(appName, appVersion, appDependencies, mainLang = JAVA).settings(
      resolvers += "Apache" at "http://repo1.maven.org/maven2/"
      ,resolvers += "jBCrypt Repository" at "http://repo1.maven.org/maven2/org/"
    )

}
