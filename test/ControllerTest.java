import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.unitils.UnitilsJUnit4;
import play.test.FakeApplication;

import static play.test.Helpers.*;

/**
 * Created with IntelliJ IDEA.
 * User: thorben
 * Date: 7/30/12
 * Time: 11:32 AM
 * To change this template use File | Settings | File Templates.
 */
public abstract class ControllerTest extends UnitilsJUnit4 {
    static Logger log = LoggerFactory.getLogger(ControllerTest.class);

    private static FakeApplication app;

    @BeforeClass
    public static void beforeClass() {
        log.error("beforeClass " + ControllerTest.class);
        if (app == null)
        {
            log.debug("starting app " + ControllerTest.class);
            app = fakeApplication(inMemoryDatabase());
            start(app);
        }

    }

    @AfterClass
    public static void afterClass() {
        log.error("afterClass " + ControllerTest.class);
//        stop(app);
    }

}
