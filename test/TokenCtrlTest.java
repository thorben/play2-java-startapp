import org.unitils.dbunit.datasetloadstrategy.impl.CleanInsertLoadStrategy;

import static play.test.Helpers.*;

import models.User;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.unitils.UnitilsJUnit4;
import org.unitils.dbunit.annotation.DataSet;
import play.mvc.Result;
import play.test.FakeApplication;

import static org.fest.assertions.Assertions.assertThat;

/**
 * Tests the TokenCtrl
 */

@DataSet(loadStrategy = CleanInsertLoadStrategy.class)
public class TokenCtrlTest extends ControllerTest {
    static Logger log = LoggerFactory.getLogger(TokenCtrlTest.class);
//    private static FakeApplication app;
//
//    @BeforeClass
//    public static void beforeClass() {
//        app = fakeApplication(inMemoryDatabase());
//        start(app);
//
//    }
//
//    @AfterClass
//    public static void afterClass() {
//        stop(app);
//    }


//    @Test
//    public void testRoutes() {
//        Router.Routes routes = app.getWrappedApplication().routes().get();
//        log.debug("something");
//        List<SqlRow> list = Ebean.createSqlQuery("select * from token for read only").findList();
//        log.info(list.toString());
//    }

    @Test
    public void testConfirmation() {
        User user = User.findByEmail("TokenCtrlTest@junit.com");
        assertThat(user).isNotNull();
        assertThat(user.validated).isFalse();
        Result result = routeAndCall(fakeRequest(GET, "/tkn/confirm/existentToken"));
        assertThat(result).isNotNull();
        assertThat(status(result)).isEqualTo(OK);
        user = User.findByEmail("TokenCtrlTest@junit.com");
        assertThat(user.validated).isTrue();

    }

    @Test
    public void testConfirmationTokenDoesNotExists() {
        Result result = routeAndCall(fakeRequest(GET, "/tkn/confirm/doesNotExist"));
        assertThat(result).isNotNull();
        assertThat(status(result)).isEqualTo(SEE_OTHER);
        assertThat(redirectLocation(result).equalsIgnoreCase("/"));
    }

    @Test
    public void testConfirmationTypeDoesNotExists() {
        Result result = routeAndCall(fakeRequest(GET, "/tkn/notExistent/existentToken"));
        assertThat(result).isNotNull();
        assertThat(status(result)).isEqualTo(BAD_REQUEST);
    }

    @Test
    public void testUserDoesNotExist() {
        Result result = routeAndCall(fakeRequest(GET, "/tkn/confirm/existentTokenWithoutUser"));
        assertThat(result).isNotNull();
        assertThat(status(result)).isEqualTo(BAD_REQUEST);
    }





}
