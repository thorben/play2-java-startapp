import controllers.routes;
import forms.Login;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.unitils.dbunit.annotation.DataSet;
import org.unitils.dbunit.datasetloadstrategy.impl.CleanInsertLoadStrategy;
import play.mvc.Result;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

import static org.fest.assertions.Assertions.assertThat;
import static play.test.Helpers.*;

/**
 * Tests the TokenCtrl
 */

@DataSet(loadStrategy = CleanInsertLoadStrategy.class)
public class AccountTest extends ControllerTest {
    static Logger log = LoggerFactory.getLogger(AccountTest.class);
    public static final String EXISTING_EMAIL = "AccountTest@junit.com";
    public static final String CORRECT_PASSWORD = "a";
    public static final String UNKNOWN_EMAIL = "unknown@junit.com";
    public static final String WRONG_PASSWORD = "b";
    public static final String EMAIL_FIELD = "email";
    public static final String PASSWORD_FIELD = "password";

    @Test
    public void testLogin() {
        Map<String, String> data = loginData(EXISTING_EMAIL, CORRECT_PASSWORD);
        Result result = routeAndCall(fakeRequest(POST, routes.Account.authenticate().url()).withFormUrlEncodedBody(data));
        assertThat(result).isNotNull();
        assertThat(status(result)).isEqualTo(SEE_OTHER);
        assertThat(session(result).get(EMAIL_FIELD)).isNotNull();
        assertThat(session(result).get(EMAIL_FIELD)).isEqualTo(EXISTING_EMAIL);
    }

    @Test
    public void testLoginWithUnknownUser() {
        Map<String, String> data = loginData(UNKNOWN_EMAIL, CORRECT_PASSWORD);
        Result result = routeAndCall(fakeRequest(POST, routes.Account.authenticate().url()).withFormUrlEncodedBody(data));
        assertThat(result).isNotNull();
        assertThat(status(result)).isEqualTo(BAD_REQUEST);
        assertThat(session(result).get(EMAIL_FIELD)).isNull();
    }

    @Test
    public void testLoginWithWrongPassword() {
        Map<String, String> data = loginData(EXISTING_EMAIL, WRONG_PASSWORD);
        Result result = routeAndCall(fakeRequest(POST, routes.Account.authenticate().url()).withFormUrlEncodedBody(data));
        assertThat(result).isNotNull();
        assertThat(status(result)).isEqualTo(BAD_REQUEST);
        assertThat(session(result).get(EMAIL_FIELD)).isNull();
    }

    @Test
    public void testLoginWithUnknownEMailAndWrongPassword() {
        Map<String, String> data = loginData(UNKNOWN_EMAIL, WRONG_PASSWORD);
        Result result = routeAndCall(fakeRequest(POST, routes.Account.authenticate().url()).withFormUrlEncodedBody(data));
        assertThat(result).isNotNull();
        assertThat(status(result)).isEqualTo(BAD_REQUEST);
        assertThat(session(result).get(EMAIL_FIELD)).isNull();
    }

    private Map<String, String> loginData(String email, String password) {
        Login login = login(email, password);
        return data(login);
    }

    private static Login login(String email, String password) {
        Login login = new Login();
        login.setEmail(email);
        login.setPassword(password);
        return login;
    }

    private Map<String,String> data(Object bean) {
        HashMap<String, String> map = new HashMap<String, String>();
        Class<? extends Object> beanClass = bean.getClass();
        Field[] fields = beanClass.getDeclaredFields();
        for (Field field : fields) {
            String name = field.getName();
            field.setAccessible(true);
            try {
                Object value = field.get(bean);
                if (value == null) {
                    map.put(name, null);
                } else {
                    map.put(name, value.toString());
                }
            } catch (IllegalAccessException e) {
                throw new RuntimeException(e);
            }
        }
        return map;
    }

}
