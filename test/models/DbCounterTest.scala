//package models
//
//import org.specs2.mutable._
//import play.api.test._
//import play.api.test.Helpers._
//import java.lang.Long
//
///**
// * @author Thorben Stangenberg (thorben@stangenberg.net}
// */
//
//class DbCounterTest extends Specification {
//
//  "the DbCounter model" should {
//    "be automatically created" in {
//      running(FakeApplication()) {
//        val nullCounter: DbCounter = DbCounter.finder.byId("default")
//        nullCounter must_== null
//        DbCounter.count("default")
//        val newCounter: DbCounter = DbCounter.finder.byId("default")
//        newCounter must_!= null
//      }
//    }
//
//    "increase on every call" in {
//      running(FakeApplication()) {
//        val count1: Long = DbCounter.count("default")
//        val count2: Long = DbCounter.count("default")
//        count2 must be equalTo(count1 + 1)
//      }
//    }
//
//  }
//
//
//
//}
