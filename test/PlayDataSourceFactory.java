import javax.sql.DataSource;
import java.util.Properties;

/**
 * Created with IntelliJ IDEA.
 * User: thorben
 * Date: 7/21/12
 * Time: 6:04 PM
 * To change this template use File | Settings | File Templates.
 */
public class PlayDataSourceFactory implements org.unitils.database.config.DataSourceFactory {
    @Override
    public DataSource createDataSource() {
        return play.db.DB.getDataSource();
    }

    @Override
    public void init(Properties properties) {
        //To change body of implemented methods use File | Settings | File Templates.
    }
}
