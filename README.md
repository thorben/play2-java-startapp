# Play2 Java Starter Application

This is a sample application using Play Framework 2.0 and Java

## Main Features
* Register
* Sign In/Out
* Reset password

## Application details
* Password is stored salted & hashed in the database
* Secure workflow to register/reset password
* Using Twitter Bootstrap
* Using Typesafe Plugin Mailer : https://github.com/typesafehub/play-plugins/tree/master/mailer
* Using Unitils DbUnit Test Framework

## Try
* Rename conf/email.conf.example in conf/email.conf and check it (smtp, etc...)
* Download & install Play Framework 2 from http://www.playframework.org/
* Open a terminal inside the application directory and execute `play run`