package forms;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import play.data.validation.Constraints;

/**
 * Registration contains user input for the registration
 */
public class Registration {
    static Logger log = LoggerFactory.getLogger(Registration.class);

    @Constraints.Required
    @Constraints.Email
    public String email;

    public String password;
}
