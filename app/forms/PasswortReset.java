package forms;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import play.data.validation.Constraints;

/**
 *  PasswortReset contains the user input to do a password reset
 */
public class PasswortReset {

    static Logger log = LoggerFactory.getLogger(PasswortReset.class);

    @Constraints.Required
    @Constraints.Email
    public String email;

}
