package forms;

import exceptions.AppException;
import models.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import play.data.validation.Constraints;
import play.i18n.Messages;

/**
 * Login contains the user input for the login
 */
public class Login {

    static Logger log = LoggerFactory.getLogger(Login.class);

    @Constraints.Required
    private String email;
    @Constraints.Required
    private String password;


    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * Validate the authentication.
     *
     * @return null if validation ok, string with details otherwise
     */
    public String validate() {

        User user = null;
        try {
            user = User.authenticate(email, password);
        } catch (AppException e) {
            return Messages.get("error.technical");
        }
        if (user == null) {
            return Messages.get("form.login.invalid");
        } else if (!user.validated) {
            return Messages.get("form.login.not_validated");
        }
        return null;
    }

}
