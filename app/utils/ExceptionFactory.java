package utils;

import exceptions.AppException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ExceptionFactory {
    static Logger log = LoggerFactory.getLogger(ExceptionFactory.class);

    public static AppException getNewAppException(Exception exception) {
        log.error(exception.getMessage(), exception);
        AppException app = new AppException();
        app.initCause(exception);
        return app;
    }
}
