package utils;

import akka.util.Duration;
import akka.util.FiniteDuration;
import com.typesafe.plugin.MailerAPI;
import com.typesafe.plugin.MailerPlugin;
import models.Token;
import org.slf4j.LoggerFactory;
import play.Configuration;
import play.api.templates.Html;
import play.libs.Akka;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Send a mail with Play20StartApp.
 */
public class Mail {

    static org.slf4j.Logger log = LoggerFactory.getLogger(Mail.class);

    /** 1 second delay on sending emails */
    private static final int DELAY = 1;

    /**
     * Envelop to prepare.
     */
    public static class Envelop {
        private String subject;
        private Html message;
        private List<String> toEmails;

        /**
         * Constructor of Envelop.
         *
         * @param subject  the subject
         * @param message  a message
         * @param toEmails list of emails adress
         */
        public Envelop(String subject, Html message, List<String> toEmails) {
            this.subject = subject;
            this.message = message;
            this.toEmails = toEmails;
        }

        public Envelop(String subject, Html message, String email) {
            this.message = message;
            this.subject = subject;
            this.toEmails = new ArrayList<String>();
            this.toEmails.add(email);
        }
    }

    public static void sendConfirmationMail(Token token) {


    }


    /**
     * Send a email, using Akka to offload it to an actor.
     *
     * @param envelop envelop to send
     */
    public static void sendMail(Envelop envelop) {
        EnvelopJob envelopJob = new EnvelopJob(envelop);
        final FiniteDuration delay = Duration.create(DELAY, TimeUnit.SECONDS);
        Akka.system().scheduler().scheduleOnce(delay, envelopJob);
    }

    static class EnvelopJob implements Runnable {
        Envelop envelop;

        public EnvelopJob(Envelop envelop) {
            this.envelop = envelop;
        }

        public void run() {
            MailerAPI email = play.Play.application().plugin(MailerPlugin.class).email();

            final Configuration root = Configuration.root();
            final String mailFrom = root.getString("mail.from");
            email.addFrom(mailFrom);
            email.setSubject(envelop.subject);
            for (String toEmail : envelop.toEmails) {
                email.addRecipient(toEmail);
                log.debug("Mail will be sent to {}", toEmail);
            }

            String message = envelop.message.body();
            log.debug("Mail Message: [{}]", message);
            email.sendHtml(message);

            log.debug("SMTP:" + root.getString("smtp.host")
                    + ":" + root.getString("smtp.port")
                    + " SSL:" + root.getString("smtp.ssl")
                    + " user:" + root.getString("smtp.user"));
        }
    }
}
