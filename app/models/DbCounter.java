package models;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import play.db.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.Id;

/**
* a database backed counter identified by a name
*/
@Entity
public class DbCounter extends Model {

    static Logger log = LoggerFactory.getLogger(DbCounter.class);

    @Id
    public String name;

    public Long count;

    public static Model.Finder<String,DbCounter> finder = new Model.Finder(String.class, DbCounter.class);

    public static Long count(String name) {
        DbCounter dbCounter = finder.byId(name);
        if (dbCounter == null) {
            dbCounter = new DbCounter();
            dbCounter.name = name;
            dbCounter.count = 0L;
        }
        dbCounter.count++;
        dbCounter.save();
        return dbCounter.count;
    }

}
