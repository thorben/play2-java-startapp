import exceptions.AppException;
import models.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import play.Application;
import play.GlobalSettings;
import play.api.mvc.Handler;
import play.mvc.Action;
import play.mvc.Http;
import play.mvc.Result;

import java.lang.reflect.Method;

/**
 * Global Application Settings
 * set in application.conf
 */
public class Global extends GlobalSettings {

    static Logger log = LoggerFactory.getLogger(Global.class);

    @Override
    public Result onError(Http.RequestHeader requestHeader, Throwable throwable) {
        log.error(throwable.getMessage(), throwable);
        return super.onError(requestHeader, throwable);
    }

    @Override
    public void beforeStart(Application application) {
        log.debug("before start {}", application);
        super.beforeStart(application);
    }

    @Override
    public void onStart(Application application) {
        log.debug("on start {}", application);
        super.onStart(application);
        if (application.isDev()) {
            setupDev(application);
        }
    }

    private void setupDev(Application application) {
        try {
            User user = User.findByEmail("thorben@stangenberg.net");
            if (user == null) {
                user = User.create("thorben@stangenberg.net", "a");
            }
            user.validated = true;
            user.save();
        } catch (AppException e) {
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    @Override
    public void onStop(Application application) {
        log.debug("stop {}", application);
        super.onStop(application);
    }

    @Override
    public Action onRequest(Http.Request request, Method method) {
        log.debug("request: {}  method: {}", request, method);
        return super.onRequest(request, method);
    }

    @Override
    public Handler onRouteRequest(Http.RequestHeader requestHeader) {
        log.debug("requestHeader: {}", requestHeader);
        return super.onRouteRequest(requestHeader);
    }

    @Override
    public Result onHandlerNotFound(Http.RequestHeader requestHeader) {
        log.debug("requestHeader: {}", requestHeader);
        return super.onHandlerNotFound(requestHeader);
    }

    @Override
    public Result onBadRequest(Http.RequestHeader requestHeader, String s) {
        log.debug("requestHeader: {}; s: {}", requestHeader, s);
        return super.onBadRequest(requestHeader, s);
    }
}


