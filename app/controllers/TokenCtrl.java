package controllers;

import exceptions.AppException;
import models.Token;
import models.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import play.mvc.Controller;
import play.mvc.Result;

/**
 * The token controller
 */
public class TokenCtrl extends Controller {

    static Logger log = LoggerFactory.getLogger(TokenCtrl.class);

    public static Result process(String tokenTypeString, String tokenString) throws AppException {
        Token.Type type;
        try {
            type = Token.Type.fromUrlPath(tokenTypeString);
        } catch (IllegalArgumentException e) {
            log.error("unknown token type [{}]", tokenTypeString);
            return badRequest("");
        }
        Token token = Token.findByTokenAndType(tokenString, type);
        if (token == null) {
            return redirect("/");
        } else {
            User user = User.finder.byId(token.userId);
            if (user == null) {
                return badRequest("");
            } else {
                user.validated = true;
                user.save();
                return ok();
            }
        }
    }

}
