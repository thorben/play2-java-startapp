package controllers;

import actions.UserLoader;
import forms.Login;
import forms.Registration;
import models.DbCounter;
import models.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import play.data.Form;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.With;

/**
 * The application controller
 */
public class Application extends Controller {
    static Logger log = LoggerFactory.getLogger(Application.class);

    @With(UserLoader.class)
    public static Result index() {
        Long count = DbCounter.count("Application.index");
        return ok(views.html.application.index.render(getUser(),"Your new application is ready.", count));
    }

    private static User getUser() {
        return (User) ctx().args.get("user");
    }

    public static Result login() {
        Form<Login> loginForm = form(Login.class);
        return ok(views.html.application.login.render(loginForm));
    }

    public static Result signOut() {
        session().clear();
        return redirect(routes.Application.index());
    }

    public static Result register() {
        Form<Registration> registerForm = form(Registration.class);
        return ok(views.html.application.registration.render(registerForm));
    }


}