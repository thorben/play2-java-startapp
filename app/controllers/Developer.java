package controllers;

import com.avaje.ebean.Ebean;
import models.DbCounter;
import models.Token;
import models.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import play.Play;
import play.mvc.Controller;
import play.mvc.Result;

/**
 * the developer controller
 */
public class Developer extends Controller {
    static Logger log = LoggerFactory.getLogger(Developer.class);

    public static Result index() {
        if (Play.isDev() || Play.isTest()) {
            return ok(views.html.developer.index.render());
        } else {
            return notFound();
        }
    }

    public static Result resetDatabase() {
        if (Play.isDev() || Play.isTest()) {
            Ebean.beginTransaction();
            Ebean.delete(User.class, User.finder.findIds());
            Ebean.delete(Token.class, Token.finder.findIds());
            Ebean.delete(DbCounter.class, DbCounter.finder.findIds());
            Ebean.commitTransaction();
            Ebean.getServerCacheManager().clearAll();
        }
        return index();
    }

}
