package controllers;

import exceptions.AppException;
import forms.Login;
import forms.PasswortReset;
import forms.Registration;
import models.Token;
import models.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import play.api.templates.Html;
import play.data.Form;
import play.i18n.Messages;
import play.mvc.Controller;
import play.mvc.Result;
import utils.Mail;

import java.net.MalformedURLException;


/**
 * the account controller
 */
public class Account extends Controller {

    static Logger log = LoggerFactory.getLogger(Account.class);

    /**
     * Handle login form submission.
     *
     * @return Dashboard if auth OK or login form if auth KO
     */
    public static Result authenticate() {
        Form<Login> loginForm = form(Login.class).bindFromRequest();
        if (loginForm.hasErrors()) {
            return badRequest(views.html.application.login.render(loginForm));
        } else {
            session("email", loginForm.get().getEmail());
            return redirect(routes.Application.index());
        }
    }

    public static Result initiatePasswordReset() {
        Form<PasswortReset> emailForm = form(PasswortReset.class).bindFromRequest();
        return ok(views.html.account.initiatePasswordReset.render(emailForm));
    }

    public static Result register() throws AppException, MalformedURLException {
        Form<Registration> registrationForm = form(Registration.class).bindFromRequest();
        if (registrationForm.hasErrors()) {
            return badRequest(views.html.application.registration.render(registrationForm));
        } else {
            Registration registration = registrationForm.get();
            User existentUser = User.findByEmail(registration.email);
            if (existentUser != null) {
                Token.sendMailResetPassword(existentUser);
            } else {
                User user = User.create(registration.email, registration.password);
                Token confirmationToken = Token.createConfirmationToken(user);
                sendConfirmationMail(confirmationToken);
            }
            return ok(views.html.account.registrationSuccess.render(registrationForm));
        }
    }

    private static void sendConfirmationMail(Token token) {
        String subject =  Messages.get("mail.reset.ask.subject");
        Html message = views.html.account.confirmationMail.render(token);
        String recipient = token.email;
        Mail.Envelop envelop = new Mail.Envelop(subject, message, recipient);
        Mail.sendMail(envelop);
    }

}