package actions;

import models.User;
import play.mvc.Action;
import play.mvc.Http;
import play.mvc.Result;

/**
 * puts the user in the current context
 */
public class UserLoader extends Action.Simple {

    @Override
    public Result call(Http.Context ctx) throws Throwable {
        String email = ctx.session().get("email");
        if (email != null) {
            User user = User.findByEmail(email);
            ctx.args.put("user", user);
        }
        return delegate.call(ctx);
    }


}
