package exceptions;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AppException extends Exception {
    static Logger log = LoggerFactory.getLogger(AppException.class);

    public AppException() {
    }

    public AppException(String message) {
        super(message);
    }

    public AppException(String message, Throwable cause) {
        super(message, cause);
    }

    public AppException(Throwable cause) {
        super(cause);
    }
}
